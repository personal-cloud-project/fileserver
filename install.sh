# Download submodules
git submodule init
git submodule update

# Dependencies
sudo apt-get -y update && sudo apt-get -y install git g++ make cmake libssl-dev perl libxcb-xinerama0-dev build-essential
sudo apt-get -y build-dep qt5-default
sudo apt-get -y install libxcb-xinerama0-dev
sudo apt-add-repository ppa:u-szeged/sedkit &&\
sudo apt-get update &&\
sudo apt-get install sedkit-env-qtwebkit
sudo apt-get -y install '^libxcb.*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev libxkbcommon-dev libxkbcommon-x11-dev libclang-dev
sudo apt-get -y install libxcursor-dev libxcomposite-dev libxdamage-dev libxrandr-dev libxtst-dev libxss-dev libdbus-1-dev libevent-dev libfontconfig1-dev libcap-dev libpulse-dev libudev-dev libpci-dev libnss3-dev libasound2-dev libegl1-mesa-dev gperf bison nodejs
sudo apt-get -y install flex bison gperf libicu-dev libxslt-dev ruby
sudo apt-get -y install libasound2-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-good1.0-dev libgstreamer-plugins-bad1.0-dev
sudo apt -y install libclang-6.0-dev llvm-6.0


# Build qt5
cd external/qt5
git checkout 5.15
#git submodule update --init --recursive
perl init-repository #-f --branch
mkdir qt5-build
cd qt5-build
../configure -developer-build -opensource -nomake examples -nomake tests -confirm-license
make -j$(nproc)
make -j$(nproc) module-qttools
cd ../../../

# Build Poco
cd external/Poco
mkdir cmake-build
cd cmake-build
cmake ..
cmake --build . --config Release
cd ../../../

# Create Database
#sudo apt install sqlite
#CREATE TABLE people(ids integer primary key, name text);
# Build Project
cmake .
make