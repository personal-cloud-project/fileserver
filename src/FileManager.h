//
// Created by panagiotis on 12/11/20.
//

#ifndef FILESERVER_FILEMANAGER_H
#define FILESERVER_FILEMANAGER_H


#include <QStringList>

class FileManager {
public:
    static QStringList listEverythingInDirectory(QString const & directory);
    static QStringList listDirsInDirectory(QString const & directory);
    static QStringList listFilesInDirectory(QString const & directory);

    /// Check if path is a subdirectory of thisDirectory
    /// \param[in] thisDirectory
    /// \param[in] path
    /// \return
    static bool isSubDirectory(const QString &thisDirectory, QString const & path);
};


#endif //FILESERVER_FILEMANAGER_H
