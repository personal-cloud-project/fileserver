//
// Created by panagiotis on 10/11/20.
//

#include <external/qt5/qt5-build/qtbase/include/QtWidgets/QMessageBox>
#include "SyncedDirectories.h"
#include "FileManager.h"

SyncedDirectories::SyncedDirectories() {
    dataBase = new DataBase();
}

QStringList SyncedDirectories::getDirectories() {
    return dataBase->getDirectories();
}

SyncedDirectories::~SyncedDirectories() {
    delete dataBase;
}

void SyncedDirectories::addDirectory(const QString &path) {
    dataBase->addDirectory(path);
}

SyncedDirectories::SyncedDirectories(const QString &connectionName) {
    dataBase = new DataBase(connectionName);
}

bool SyncedDirectories::isPathSynced(const QString &path) {
    QStringList syncedDirectories = getDirectories();

    return std::any_of(syncedDirectories.begin(), syncedDirectories.end(),
                       [path](QString const & dir) { return  FileManager::isSubDirectory(dir, path);});
}

void SyncedDirectories::removeDirectory(const QString &path) {
    dataBase->deleteDirectory(path);
}
