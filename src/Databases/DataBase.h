//
// Created by panagiotis on 10/11/20.
//

#ifndef FILESERVER_DATABASE_H
#define FILESERVER_DATABASE_H

#include <QtSql>

class DataBase {
public:
    DataBase();
    DataBase(QString const & connectionName);

    ~DataBase();

    void addDirectory(QString const &path);
    void deleteDirectory(QString const & path);
    QStringList getDirectories();

private:
    QString connectionName;
    QSqlDatabase db;

    const QString DIRECTORIES_SQL = QLatin1String(R"(
    create table directories(id integer primary key, path varchar)
    )");

    const QString DELETE_DIRECTORY_SQL = QLatin1String(R"(
    DELETE FROM directories WHERE path=')");

    const QString INSERT_DIRECTORY_SQL = QLatin1String(R"(
    insert into directories(path) values(?)
    )");

    QSqlError initDB();
};


#endif //FILESERVER_DATABASE_H
