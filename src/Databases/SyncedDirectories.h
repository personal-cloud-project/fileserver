//
// Created by panagiotis on 10/11/20.
//

#ifndef FILESERVER_SYNCEDDIRECTORIES_H
#define FILESERVER_SYNCEDDIRECTORIES_H

#include <QtSql>
#include "DataBase.h"

class SyncedDirectories : public QObject {
Q_OBJECT

public:
    SyncedDirectories();
    SyncedDirectories(QString const & connectionName);
    QStringList getDirectories();
    ~SyncedDirectories();

    void addDirectory(QString const & path);
    void removeDirectory(QString const & path);

    /// Check if path is one of the synced directories
    /// or one of their subdirectories.
    /// \param path
    /// \return
    bool isPathSynced(QString const & path);

private:
    DataBase* dataBase;
};


#endif //FILESERVER_SYNCEDDIRECTORIES_H
