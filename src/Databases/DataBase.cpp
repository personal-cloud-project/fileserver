//
// Created by panagiotis on 10/11/20.
//

#include "DataBase.h"
#include <QSqlDatabase>
#include <iostream>

void DataBase::addDirectory(QString const &path) {
    initDB();

    QSqlQuery query(db);
    query.prepare(INSERT_DIRECTORY_SQL);
//    query.bindValue(path);
    query.addBindValue(path);
    query.exec();
}

QSqlError DataBase::initDB() {
    if (QSqlDatabase::contains(connectionName))
        return QSqlError();

    db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
    db.setDatabaseName("fileserverDatabase");

    if (db.isOpen())
        return QSqlError();

    if (!db.open())
        return db.lastError();

    QStringList tables = db.tables();
    if (tables.contains("directories", Qt::CaseInsensitive))
        return QSqlError();

    QSqlQuery q;
    if (!q.exec(DIRECTORIES_SQL))
        return q.lastError();

    return QSqlError();
}

QStringList DataBase::getDirectories() {
    initDB();

    QStringList qStringList;
    QSqlQuery query("SELECT * FROM directories", db);

    int idName = query.record().indexOf("path");

    while (query.next())
    {
        QString name = query.value(idName).toString();
        qStringList.push_back(name);
    }
    return qStringList;
}

DataBase::DataBase() {
    connectionName = QString("qt_sql_default_connection");
    initDB();
}

DataBase::~DataBase() {
    if (db.isOpen())
        db.close();

    if (QSqlDatabase::contains(connectionName))
        QSqlDatabase::removeDatabase(connectionName);
}

DataBase::DataBase(QString const & connectionName) {
    this->connectionName = connectionName;
    initDB();
}

void DataBase::deleteDirectory(const QString &path) {
    initDB();

    QSqlQuery query(db);
    query.prepare(DELETE_DIRECTORY_SQL + path + "'");
    query.exec();
}
