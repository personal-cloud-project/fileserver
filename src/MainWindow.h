//
// Created by panagiotis on 3/11/20.
//

#ifndef CLIENT_MAINWINDOW_H
#define CLIENT_MAINWINDOW_H

#include <QWidget>
#include <QDir>
#include <qt5-build/qtbase/include/QtWidgets/QComboBox>
#include <qt5-build/qtbase/include/QtWidgets/QLabel>
#include <qt5-build/qtbase/include/QtWidgets/QPushButton>
#include <qt5-build/qtbase/include/QtCore/QDir>
#include <qt5-build/qtbase/include/QtWidgets/QTableWidget>
#include <src/Net/MyHttpServer.h>

QT_BEGIN_NAMESPACE
class QComboBox;
class QLabel;
class QPushButton;
class QTableWidget;
class QTableWidgetItem;
QT_END_NAMESPACE

class MainWindow : public QWidget {
public:
    explicit MainWindow(QWidget* parent = nullptr);

private slots:

    void browse();
    void find();
    void animateFindClick();
    void openFileOfItem(int row, int column);
    void contextMenu(const QPoint &pos);
    void handleHttpServerButton();

private:
    HTTPServerThread* httpServerThread;
    SyncedDirectories* syncedDirectories;

    QStringList findFiles(const QStringList &files, const QString &text);
    void showFiles(const QStringList &paths);
    QComboBox *createComboBox(const QString &text = QString());
    void createFilesTable();

    QComboBox *fileComboBox;
    QComboBox *textComboBox;
    QComboBox *directoryComboBox;
    QLabel *filesFoundLabel;
    QTableWidget *filesTable;

    QDir currentDir;

    void removePathFromSyncedDirectories(QString const & path);
};


#endif //CLIENT_MAINWINDOW_H
