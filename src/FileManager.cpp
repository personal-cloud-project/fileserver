//
// Created by panagiotis on 12/11/20.
//

#include "FileManager.h"
#include <QDirIterator>
#include <iostream>

QStringList FileManager::listEverythingInDirectory(const QString &directory) {
    QDirIterator it(directory, QDir::AllEntries | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    QStringList files;

    while (it.hasNext())
        files.push_back(it.next());

    return files;
}

QStringList FileManager::listDirsInDirectory(const QString &directory) {
    QDirIterator it(directory, QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    QStringList files;

    while (it.hasNext())
        files.push_back(it.next());

    return files;
}

QStringList FileManager::listFilesInDirectory(const QString &directory) {
    QDirIterator it(directory, QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
    QStringList files;

    while (it.hasNext())
        files.push_back(it.next());

    return files;
}

bool FileManager::isSubDirectory(const QString &thisDirectory, const QString &path) {
    QStringList thisDirComponents = thisDirectory.split(QDir::separator());


    // evaluate path , e.g. it may contain /../
    QDir dir(path);
    QStringList pathComponents = dir.absolutePath().split(QDir::separator());

    QStringList::iterator pathIt = pathComponents.begin();

    // check if the first components of thisDirectory match
    // with those of path
    for (QString baseComponent : thisDirComponents) {
        // this check needs to be here DON'T MOVE
        if (pathIt == pathComponents.end())
            return false;

        if (baseComponent.compare(*pathIt) != 0)
            return false;

        pathIt++;
    }

    return true;
}

