//
// Created by panagiotis on 5/11/20.
//

#include <qt5-build/qtbase/include/QtCore/QStringList>
#include <qt5-build/qtbase/include/QtCore/QFile>
#include <fstream>
#include <qt5-build/qtbase/include/QtCore/QTextStream>
#include <qt5-build/qtbase/include/QtCore/QDir>
#include "Settings.h"

int Settings::add_synced_directory(QString current_directory, QString synced_directory) {
    QFile file(this->SYNCED_FOLDERS_FILE);

    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        QTextStream out(&file);
        QDir absoluteFilePath(current_directory);
        out << absoluteFilePath.absoluteFilePath(synced_directory) << "\n";
        file.close();
        return 0;
    }

    return 1;
}

QStringList Settings::get_synced_directories() {
    QStringList stringsList;
    QFile file(this->SYNCED_FOLDERS_FILE);

    if (file.open(QIODevice::ReadOnly)) {
        QTextStream in(&file);

        while (!in.atEnd()) {
            stringsList.push_back(in.readLine());
        }

        file.close();
    }

    return stringsList;
}
