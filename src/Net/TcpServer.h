//
// Created by panagiotis on 8/11/20.
//

#ifndef CLIENT_TCPSERVER_H
#define CLIENT_TCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <iostream>

class TcpServer : public QObject {
    Q_OBJECT
public:
    explicit TcpServer(QObject *parent = nullptr, unsigned int port = 9999);

    typedef enum {FETCH_FILES_LIST = 1} ServerRequests;

signals:

public slots:
    void onNewConnection();
    void onReadyRead();
private:
    QTcpServer* qTcpServer;
    unsigned int port;

    int getServerRequest(QTcpSocket *qTcpSocket) const;

    void sendFilesList(QTcpSocket *qTcpSocket) const;
};


#endif //CLIENT_TCPSERVER_H
