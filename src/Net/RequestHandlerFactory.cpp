//
// Created by panagiotis on 12/11/20.
//

#include "RequestHandlerFactory.h"
#include "DownloadFileHandler.h"

RequestHandlerFactory::RequestHandlerFactory() {

}

HTTPRequestHandler *RequestHandlerFactory::createRequestHandler(const HTTPServerRequest &request) {
    if (request.getURI() == "/" )//|| request.getURI().rfind("/?path", 0) == 0)
        return new ListDirectoriesPageHandler();
    else if (request.getURI() == "/get" || request.getURI().rfind("/get", 0) == 0)
        return new DownloadFileHandler();
    else
        return nullptr;
}
