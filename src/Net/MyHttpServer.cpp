//
// Created by panagiotis on 11/11/20.
//

#include "MyHttpServer.h"

void MyHttpServer::stop() {
    if (srv != nullptr) {
//            terminate();
        srv->stop();
    }
}

void MyHttpServer::initialize(Application &self) {
    loadConfiguration(); // load default configuration files, if present
    ServerApplication::initialize(self);
}

void MyHttpServer::uninitialize() {
    ServerApplication::uninitialize();
}

void MyHttpServer::defineOptions(OptionSet &options) {
    ServerApplication::defineOptions(options);

    options.addOption(
            Option("help", "h", "display help information on command line arguments")
                    .required(false)
                    .repeatable(false));
}

void MyHttpServer::handleOption(const std::string &name, const std::string &value) {
    ServerApplication::handleOption(name, value);

    if (name == "help")
        _helpRequested = true;
}

void MyHttpServer::displayHelp() {
    HelpFormatter helpFormatter(options());
    helpFormatter.setCommand(commandName());
    helpFormatter.setUsage("OPTIONS");
    helpFormatter.setHeader("A web server that serves the current date and time.");
    helpFormatter.format(std::cout);
}

int MyHttpServer::main(const std::vector<std::string> &args) {
    if (_helpRequested)
    {
        displayHelp();
    }
    else
    {
        // get parameters from configuration file
        unsigned short port = (unsigned short) config().getInt("HTTPTimeServer.port", 9980);
        int maxQueued  = config().getInt("HTTPTimeServer.maxQueued", 100);
        int maxThreads = config().getInt("HTTPTimeServer.maxThreads", 16);
        ThreadPool::defaultPool().addCapacity(maxThreads);

        HTTPServerParams* pParams = new HTTPServerParams;
        pParams->setMaxQueued(maxQueued);
        pParams->setMaxThreads(maxThreads);

        // set-up a server socket
        ServerSocket svs(port);
        // set-up a HTTPServer instance
        srv = new HTTPServer(new RequestHandlerFactory(), svs, pParams);
        // start the HTTPServer
        srv->start();
        // wait for CTRL-C or kill
//            waitForTerminationRequest();
        // Stop the HTTPServer
//            srv->stop();
    }
    return Application::EXIT_OK;
}
