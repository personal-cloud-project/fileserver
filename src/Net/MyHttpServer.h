//
// Created by panagiotis on 11/11/20.
//

#ifndef FILESERVER_MYHTTPSERVER_H
#define FILESERVER_MYHTTPSERVER_H

#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/Exception.h"
#include "Poco/ThreadPool.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "src/Databases/SyncedDirectories.h"
#include "ListDirectoriesPageHandler.h"
#include <iostream>
#include "RequestHandlerFactory.h"

using Poco::Net::ServerSocket;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServerParams;
using Poco::Timestamp;
using Poco::DateTimeFormatter;
using Poco::DateTimeFormat;
using Poco::ThreadPool;
using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::HelpFormatter;


class MyHttpServer: public Poco::Util::ServerApplication
    /// The main application class.
    ///
    /// This class handles command-line arguments and
    /// configuration files.
    /// Start the HTTPTimeServer executable with the help
    /// option (/help on Windows, --help on Unix) for
    /// the available command line options.
    ///
    /// To use the sample configuration file (HTTPTimeServer.properties),
    /// copy the file to the directory where the HTTPTimeServer executable
    /// resides. If you start the debug version of the HTTPTimeServer
    /// (HTTPTimeServerd[.exe]), you must also create a copy of the configuration
    /// file named HTTPTimeServerd.properties. In the configuration file, you
    /// can specify the port on which the server is listening (default
    /// 9980) and the format of the date/time string sent back to the client.
    ///
    /// To test the TimeServer you can use any web browser (http://localhost:9980/).
{
public:
    MyHttpServer(): _helpRequested(false)
    {
        srv = nullptr;
    }

    ~MyHttpServer()
    {
        delete srv;
    }

    void stop();

protected:
    HTTPServer* srv;

    void initialize(Application& self);

    void uninitialize();

    void defineOptions(OptionSet& options);

    void handleOption(const std::string& name, const std::string& value);

    void displayHelp();

    int main(const std::vector<std::string>& args);

private:
    bool _helpRequested;
};

class HTTPServerThread : public QThread
{
Q_OBJECT

public:
    MyHttpServer* server;

    void run() {
        server = new MyHttpServer();
        char** argv;
        argv =  new char*[1];
        char text[11] = "fileServer";
        argv[0] = text;
        server->run(1, argv);
//        delete [] argv;
    };

    HTTPServerThread() {
        server = nullptr;
    };

    ~HTTPServerThread() override {
        if (server != nullptr) {
//            server->stop();
            delete server;
        }
    };

    void stopSrver() {
        server->stop();
    };
};

#endif //FILESERVER_MYHTTPSERVER_H
