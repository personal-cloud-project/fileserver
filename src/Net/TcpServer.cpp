//
// Created by panagiotis on 8/11/20.
//

#include "TcpServer.h"
#include "Settings.h"
#include <iostream>
#include <QIODevice>

TcpServer::TcpServer(QObject *parent, unsigned int port) : QObject(parent), port(port) {
    qTcpServer = new QTcpServer(this);

    // emit signal when a user connects
    connect(qTcpServer, SIGNAL(newConnection()), this, SLOT(onNewConnection()));

    if (!qTcpServer->listen(QHostAddress::Any, port))
        exit(EXIT_FAILURE);
}

void TcpServer::onNewConnection() {
    // grab the socket
    QTcpSocket* qTcpSocket = qTcpServer->nextPendingConnection();
    connect(qTcpSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

    qTcpSocket->write("HTTP/1.1 200 ok\r\n");
    qTcpSocket->write("Connection: keep alive\r\n");
    qTcpSocket->write("Content-Length: 4\r\n");
    qTcpSocket->write("\r\n");
    qTcpSocket->write("test");
}

void TcpServer::onReadyRead() {
    QTcpSocket* qTcpSocket = static_cast<QTcpSocket*>(QObject::sender());

    QByteArray array = qTcpSocket->readAll();
    std::cout << array.toStdString() << "\n";
    return;

    // read request code
    int request = getServerRequest(qTcpSocket);

//    std::cout << request;fflush(stdout);

    switch (request) {
        case FETCH_FILES_LIST:
            sendFilesList(qTcpSocket);
    }


    qTcpSocket->flush();
    qTcpSocket->waitForBytesWritten(3000);
    qTcpSocket->close();

}


void TcpServer::sendFilesList(QTcpSocket *qTcpSocket) const {
    Settings settings;
    QStringList filesList = settings.get_synced_directories();
    for (const QString& file : filesList)
        qTcpSocket->write((file.toStdString() + "\n").c_str());
}

int TcpServer::getServerRequest(QTcpSocket *qTcpSocket) const {
    char c;
    qTcpSocket->getChar(&c);
    int request = c - '0';
    return request;
}
