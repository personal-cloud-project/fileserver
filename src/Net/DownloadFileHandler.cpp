//
// Created by panagiotis on 12/11/20.
//

#include "DownloadFileHandler.h"
#include "src/Databases/SyncedDirectories.h"
#include "FileManager.h"

void DownloadFileHandler::handleRequest(HTTPServerRequest &request, HTTPServerResponse &response) {
    Application& app = Application::instance();
    app.logger().information("Download Request from " + request.clientAddress().toString() + " " + request.getURI());

    response.sendFile(std::string(request.getURI()).erase(0, 4), "application/x-www-form-urlencoded");

    // TODO check is it is a path in database
    // TODO spaces
    if (request.has("path"))
        response.sendFile(request.get("path"), "");
}

DownloadFileHandler::DownloadFileHandler() {

}
