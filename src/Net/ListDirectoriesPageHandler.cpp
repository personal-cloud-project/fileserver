//
// Created by panagiotis on 12/11/20.
//

#include "ListDirectoriesPageHandler.h"
#include "src/Databases/SyncedDirectories.h"
#include "FileManager.h"

const char* LOAD_DIR_FUNCTION = "function loadDir(dir) "
                                "{"
                                "\n"
                                "var data = null;\n"
                                "var xhr = new XMLHttpRequest();\n"
                                "xhr.withCredentials = true;\n"
                                "\n"
                                "xhr.addEventListener(\"readystatechange\", function () {\n"
                                "  if (this.readyState === 4) {\n"
                                "document.getElementById(\"listdiv\").innerHTML = this.responseText;\n"
                                "  }\n"
                                "});\n"
                                "\n"
                                "xhr.open(\"GET\", \"http://localhost:9980/\");\n"
                                "xhr.setRequestHeader(\"path\", dir);\n"
                                "xhr.setRequestHeader(\"cache-control\", \"no-cache\");\n"
                                "\n"
                                "xhr.send(data);\n"
                                "}\n";



void ListDirectoriesPageHandler::handleRequest(HTTPServerRequest &request, HTTPServerResponse &response) {
    Application& app = Application::instance();
    app.logger().information("Request from " + request.clientAddress().toString());

    response.setChunkedTransferEncoding(true);
    response.setContentType("text/html");
    std::ostream& ostr = response.send();

    ostr << "<html><head><title>Fileserver</title>";
    ostr << "<script>\n"
            "\n"
            << LOAD_DIR_FUNCTION <<
            "\n"
            "</script>";
    ostr << "</head>";
    ostr << "<body><div id='listdiv'>";

    if (request.has("path")) {
        listEverythingInPath(request, ostr);
    }
    else {
        listSyncedDirectories(ostr);
    }

    ostr << "</div></body></html>";
}

void ListDirectoriesPageHandler::listSyncedDirectories(std::ostream &ostr) const {
    QStringList directories;
    SyncedDirectories syncedDirectories("my_db" + QString::number((quint64) QThread::currentThread(), 16));

    directories = syncedDirectories.getDirectories();

    for (const QString& directory : directories)
        ostr << "<p>"
                "<a href=\"#\" onclick=\"loadDir('" << directory.toStdString() << "')\">" << directory.toStdString() << "</a>"
                "</p>";
}

void ListDirectoriesPageHandler::listEverythingInPath(const HTTPServerRequest &request, std::ostream &ostr) const {
    QString path = QString(request.get("path").c_str());

    QStringList directories, files;
    SyncedDirectories syncedDirectories("my_db" + QString::number((quint64) QThread::currentThread(), 16));

    if (!syncedDirectories.isPathSynced(path)) {
        ostr << R"(<p>Not allowed path.</p>)";
        return;
    }

    directories = FileManager::listDirsInDirectory(path);
    files = FileManager::listFilesInDirectory(path);

    for (const QString& directory : directories)
        ostr << "<p>"
                "<a href=\"#\" onclick=\"loadDir('" << directory.toStdString() << "')\">" << directory.toStdString() << "</a>"
                                                                                                                        "</p>";

    for (const QString& file : files)
        ostr << R"(<form method="POST" action="get)" << file.toStdString() << R"(">
                   <button type="submit">)" << file.toStdString() << R"(</button>
                    </form>)";
}


ListDirectoriesPageHandler::ListDirectoriesPageHandler() {

}
