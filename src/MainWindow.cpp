//
// Created by panagiotis on 3/11/20.
//

#include <qt5-build/qtbase/include/QtWidgets/QGridLayout>
#include <qt5-build/qtbase/include/QtWidgets/QFileDialog>
#include <qt5-build/qtbase/include/QtGui/QDesktopServices>
#include <qt5-build/qtbase/include/QtCore/QDirIterator>
#include <qt5-build/qtbase/include/QtWidgets/QProgressDialog>
#include <qt5-build/qtbase/include/QtCore/QMimeDatabase>
#include <qt5-build/qtbase/include/QtCore/QCoreApplication>
#include <qt5-build/qtbase/include/QtGui/QGuiApplication>
#include <qt5-build/qtbase/include/QtWidgets/QMenu>
#include "MainWindow.h"
#include "src/Databases/SyncedDirectories.h"
#include <QtWidgets>
#include <src/UiElements/UiSwitch.h>
#include "MyHttpServer.h"

enum { absoluteFileNameRole = Qt::UserRole + 1 };
//! [17]

//! [18]
static inline QString fileNameOfItem(const QTableWidgetItem *item)
{
    return item->data(absoluteFileNameRole).toString();
}
//! [18]

//! [14]
static inline void openFile(const QString &fileName)
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(fileName));
}

MainWindow::MainWindow(QWidget *parent) : QWidget(parent) {
    syncedDirectories = new SyncedDirectories();

    setWindowTitle("File Server");

    QPushButton *browseButton = new QPushButton(QString("Publish directory"));//tr("&Browse..."), this);
    connect(browseButton, &QAbstractButton::clicked, this, &MainWindow::browse);
//    findButton = new QPushButton(tr("&Find"), this);
//    connect(findButton, &QAbstractButton::clicked, this, &BrowseDirectoryWindow::find);

//    fileComboBox = createComboBox(tr("*"));
//    connect(fileComboBox->lineEdit(), &QLineEdit::returnPressed,
//            this, &MainWindow::animateFindClick);
//    textComboBox = createComboBox();
//    connect(textComboBox->lineEdit(), &QLineEdit::returnPressed,
//            this, &MainWindow::animateFindClick);
//    directoryComboBox = createComboBox(QDir::toNativeSeparators(QDir::currentPath()));
//    connect(directoryComboBox->lineEdit(), &QLineEdit::returnPressed,
//            this, &MainWindow::animateFindClick);
//
//    filesFoundLabel = new QLabel;


    QGridLayout *mainLayout = new QGridLayout(this);

    UiSwitch* switch1 = new UiSwitch("HTTP Server", this);
    connect(switch1, &QAbstractButton::released, this, &MainWindow::handleHttpServerButton);

//    mainLayout->addWidget(switch1);

    createFilesTable();

    mainLayout->addWidget(switch1, 0, 0);
    mainLayout->addWidget(browseButton, 3, 0, Qt::AlignTop);
    mainLayout->addWidget(filesTable, 3, 1);
//    mainLayout->addWidget(new QLabel(tr("Named:")), 0, 0);
//    mainLayout->addWidget(fileComboBox, 0, 1, 1, 2);
//    mainLayout->addWidget(new QLabel(tr("Containing text:")), 1, 0);
//    mainLayout->addWidget(textComboBox, 1, 1, 1, 2);
//    mainLayout->addWidget(new QLabel(tr("In directory:")), 2, 0);
//    mainLayout->addWidget(directoryComboBox, 2, 1);

//    mainLayout->addWidget(filesFoundLabel, 4, 0, 1, 2);
//    mainLayout->addWidget(findButton, 4, 2);
//    Settings settings;
//    showFiles(settings.get_synced_directories());
    QStringList qStringList = syncedDirectories->getDirectories();
    showFiles(qStringList);
    httpServerThread = nullptr;
}

void MainWindow::browse()
{
    QString directory =
            QDir::toNativeSeparators(QFileDialog::getExistingDirectory(this, tr("Find Files"), QDir::currentPath()));

    if (!directory.isEmpty()) {
//        if (directoryComboBox->findText(directory) == -1)
//            directoryComboBox->addItem(directory);
//        directoryComboBox->setCurrentIndex(directoryComboBox->findText(directory));

        QDir absoluteFilePath(currentDir.path());

        syncedDirectories->addDirectory(absoluteFilePath.absoluteFilePath(directory));
        filesTable->setRowCount(0);
        showFiles(syncedDirectories->getDirectories());
    }
}

static void updateComboBox(QComboBox *comboBox)
{
    if (comboBox->findText(comboBox->currentText()) == -1)
        comboBox->addItem(comboBox->currentText());
}


void MainWindow::find()
{
    filesTable->setRowCount(0);

    QString fileName = fileComboBox->currentText();
    QString text = textComboBox->currentText();
    QString path = QDir::cleanPath(directoryComboBox->currentText());
    currentDir = QDir(path);


    updateComboBox(fileComboBox);
    updateComboBox(textComboBox);
    updateComboBox(directoryComboBox);


    QStringList filter;
    if (!fileName.isEmpty())
        filter << fileName;
    QDirIterator it(path, filter, QDir::AllEntries | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
    QStringList files;
    while (it.hasNext())
        files << it.next();
    if (!text.isEmpty())
        files = findFiles(files, text);
    files.sort();
    showFiles(files);
}


void MainWindow::animateFindClick()
{
}


QStringList MainWindow::findFiles(const QStringList &files, const QString &text)
{
    QProgressDialog progressDialog(this);
    progressDialog.setCancelButtonText(tr("&Cancel"));
    progressDialog.setRange(0, files.size());
    progressDialog.setWindowTitle(tr("Find Files"));

    QMimeDatabase mimeDatabase;
    QStringList foundFiles;

    for (int i = 0; i < files.size(); ++i) {
        progressDialog.setValue(i);
        progressDialog.setLabelText(tr("Searching file number %1 of %n...", nullptr, files.size()).arg(i));
        QCoreApplication::processEvents();

        if (progressDialog.wasCanceled())
            break;

        const QString fileName = files.at(i);
        const QMimeType mimeType = mimeDatabase.mimeTypeForFile(fileName);
        if (mimeType.isValid() && !mimeType.inherits(QStringLiteral("text/plain"))) {
            qWarning() << "Not searching binary file " << QDir::toNativeSeparators(fileName);
            continue;
        }
        QFile file(fileName);
        if (file.open(QIODevice::ReadOnly)) {
            QString line;
            QTextStream in(&file);
            while (!in.atEnd()) {
                if (progressDialog.wasCanceled())
                    break;
                line = in.readLine();
                if (line.contains(text, Qt::CaseInsensitive)) {
                    foundFiles << files[i];
                    break;
                }
            }
        }
    }
    return foundFiles;
}


void MainWindow::showFiles(const QStringList &paths)
{
    for (const QString &filePath : paths) {
        const QString toolTip = QDir::toNativeSeparators(filePath);
        const QString absolutePath = QDir::toNativeSeparators(currentDir.absoluteFilePath(filePath));
        const qint64 size = QFileInfo(filePath).size();
        QTableWidgetItem *fileNameItem = new QTableWidgetItem(absolutePath);
        fileNameItem->setData(absoluteFileNameRole, QVariant(filePath));
        fileNameItem->setToolTip(toolTip);
        fileNameItem->setFlags(fileNameItem->flags() ^ Qt::ItemIsEditable);
//        QTableWidgetItem *sizeItem = new QTableWidgetItem(QLocale().formattedDataSize(size));
//        sizeItem->setData(absoluteFileNameRole, QVariant(filePath));
//        sizeItem->setToolTip(toolTip);
//        sizeItem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
//        sizeItem->setFlags(sizeItem->flags() ^ Qt::ItemIsEditable);

        int row = filesTable->rowCount();
        filesTable->insertRow(row);
        filesTable->setItem(row, 0, fileNameItem);
//        filesTable->setItem(row, 1, sizeItem);
    }
//    filesFoundLabel->setText(tr("%n file(s) found (Double click on a file to open it)", nullptr, paths.size()));
//    filesFoundLabel->setWordWrap(true);
}


QComboBox *MainWindow::createComboBox(const QString &text)
{
    QComboBox *comboBox = new QComboBox;
    comboBox->setEditable(true);
    comboBox->addItem(text);
    comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    return comboBox;
}


void MainWindow::createFilesTable()
{
    filesTable = new QTableWidget(0, 1);
    filesTable->setSelectionBehavior(QAbstractItemView::SelectRows);

    QStringList labels;
    labels << tr("Published Directories");// << tr("Size");
    filesTable->setHorizontalHeaderLabels(labels);
    filesTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    filesTable->verticalHeader()->hide();
    filesTable->setShowGrid(false);
    filesTable->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(filesTable, &QTableWidget::customContextMenuRequested,
            this, &MainWindow::contextMenu);
    connect(filesTable, &QTableWidget::cellActivated,
            this, &MainWindow::openFileOfItem);
}



void MainWindow::openFileOfItem(int row, int /* column */)
{
    const QTableWidgetItem *item = filesTable->item(row, 0);
    openFile(fileNameOfItem(item));
}


void MainWindow::contextMenu(const QPoint &pos)
{
    const QTableWidgetItem *item = filesTable->itemAt(pos);
    if (!item)
        return;
    QMenu menu;
    QAction *removePath = menu.addAction("Remove Path");
#ifndef QT_NO_CLIPBOARD
    QAction *copyAction = menu.addAction("Copy Name");
#endif
    QAction *openAction = menu.addAction("Open");
    QAction *action = menu.exec(filesTable->mapToGlobal(pos));
    if (!action)
        return;
    const QString fileName = fileNameOfItem(item);
    if (action == openAction)
        openFile(fileName);
#ifndef QT_NO_CLIPBOARD
    else if (action == copyAction)
        QGuiApplication::clipboard()->setText(QDir::toNativeSeparators(fileName));
    else if (action == removePath) {
        removePathFromSyncedDirectories(fileName);
        filesTable->setRowCount(0);
        showFiles(syncedDirectories->getDirectories());
    }
#endif
}


void MainWindow::handleHttpServerButton() {
    if (httpServerThread != nullptr) {
        httpServerThread->stopSrver();
        httpServerThread->terminate();
        httpServerThread->wait();
        httpServerThread->exit(0);
        delete httpServerThread;
        httpServerThread = nullptr;
        std::cout << "here\n";
    }
    else {
        httpServerThread = new HTTPServerThread;
        httpServerThread->start();
        std::cout << "here1\n";
    }
}


void MainWindow::removePathFromSyncedDirectories(QString const & path) {
    syncedDirectories->removeDirectory(path);
}

