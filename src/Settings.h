//
// Created by panagiotis on 5/11/20.
//

#ifndef CLIENT_SETTINGS_H
#define CLIENT_SETTINGS_H

#include <list>
#include <QString>

class Settings {
    const char* SYNCED_FOLDERS_FILE = "synced_folders";

public:
    Settings() = default;;

    /// Inserts a new record to SYNCED_FOLDERS_FILE
    /// \param current_directory The current directory (needed to fix paths)
    /// \param synced_directory The new record
    /// \return 0 if success, non-zero otherwise
    int add_synced_directory(QString current_directory, QString synced_directory);

    /// Get all records from SYNCED_FOLDERS_FILE
    /// \return a list with all synced directories
    QStringList get_synced_directories();


};


#endif //CLIENT_SETTINGS_H
